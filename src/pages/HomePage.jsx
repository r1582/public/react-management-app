import React, { Fragment } from 'react'
import Dashboard from '../components/Dashboard/Dashboard'
import Header from '../components/Header/Header'

const HomePage = () => {
    return (
        <Fragment>
            <Header></Header>
            <Dashboard></Dashboard>
        </Fragment>
    )
}

export default HomePage
