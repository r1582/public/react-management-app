import './App.css';
import AppRoute from './router/AppRoute';
import { BrowserRouter } from "react-router-dom";
import { Fragment } from 'react';

function App() {
  return (
    <Fragment>
      <BrowserRouter>
        <AppRoute></AppRoute>
      </BrowserRouter>
    </Fragment>
  );
}

export default App;
