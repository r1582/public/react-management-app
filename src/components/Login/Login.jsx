import React, { Fragment, useState } from 'react'
import { Container, Form, Button } from 'react-bootstrap'

const Login = () => {

  const loginData = [
      {uemail: "martin@gmail.com", upassword: "12345"}
  ]

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function handleSubmit(event) {
    event.preventDefault();
  }

  const emailHandle=(e)=>{
    setEmail(e.target.value)
  }
  const passHandle=(e)=>{
    setPassword(e.target.value)
  }

  

  console.log(email);


    return (
        <Fragment>
            <h1 className="title">Login Here</h1>
            <Container>
                <Form className='small-form' onSubmit={handleSubmit}>
                    <Form.Group className="mb-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={email} onChange={emailHandle}/>
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" value={password} onChange={passHandle}/>
                    </Form.Group>
                    <Button variant="primary" type="submit">Submit</Button>
                </Form>
            </Container>
        </Fragment>
    )
}

export default Login
