import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Col, Container, Row, Table } from 'react-bootstrap';

const Dashboard = () => {

    const person = [
        {name: "Tasnimul Tamal", status: "Pending"},
        {name: "Sayed Masud", status: "Success"},
        {name: "Moksedul Islam", status: "Pending"},
        {name: "John Alex", status: "Pending"},
        {name: "Natalia Faustufa", status: "Success"},
        {name: "John", status: "Pending"}
    ]

    const [info, setInfo] = useState([]);

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/users').then(response=>{
            setInfo(response.data);
        }).catch(error=>{
            alert("Data not found!!!");
        })
    }, []);


    return (
        <div>
            <h1 className="text-center py-5">Dashboard</h1>
            <Container>
                <Row>
                    <Col lg={6}>
                        {
                            info.length > 0 &&
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        info.map((result)=>(
                                            <tr>
                                                <td>{result.name}</td>
                                                <td>{result.email}</td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </Table>
                        }
                    
                    </Col>
                    <Col lg={6}>
                        <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        person.map((result)=>(
                                            <tr>
                                                <td>{result.name}</td>
                                                <td className={result.status == 'Pending' ? "bg-danger": "bg-success"}>{result.status}</td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </Table>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Dashboard
